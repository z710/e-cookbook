﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_cookbook.Models
{
    public class Recipe
    {

        public string Id { get; set; }
        public string Title { get; set; }
        public DateTime Createdat { get; set; }
        public DateTime Lastedit { get; set; }
        public string UserId { get; set; }
        
        
        public List<User> User { get; set; }
        public List<Comment> Comment { get; set; }
        public RecipeRating RecipeRating { get; set; }
        public List<RecipeStep> RecipeStep { get; set; }
        public List<RecipeIngredient> RecipeIngredients { get; set; }

       public List<Ingredient> Ingredients { get; set; }
      
    }
}
