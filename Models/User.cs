using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace e_cookbook.Models
{
    public class User : IdentityUser
    {
        public Recipe Recipe { get; set; }
    }
}