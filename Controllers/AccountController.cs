using System;
using System.Threading.Tasks;
using e_cookbook.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace e_cookbook.Controllers
{
    [Route("[controller]")]
    public class AccountController : Controller
    {
        private EcookbookContext _ecookbookContext;
        private UserManager<User> _userManager;
        private SignInManager<User> _signInManager;
        private RoleManager<IdentityRole> _roleManager;

        public AccountController(EcookbookContext context, UserManager<User> userManager,
            SignInManager<User> signInManager, RoleManager<IdentityRole> roleManager)
        {
            _ecookbookContext = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }

        [HttpGet]
        [Route("")]
        [Authorize]
        public async Task<IActionResult> Profile()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;
            ViewBag.email = user.Email;
            return View();
        }
        
        [HttpGet]
        [Route("login")]
        public async Task<IActionResult> Login(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromForm]User user ,string returnUrl)
        {
            
            var result = await _signInManager.PasswordSignInAsync(user.UserName, user.PasswordHash, true, true);
            if (result.IsLockedOut)
            {
                return Content("Account is locked", "text/html");
            }

            if (result.IsNotAllowed)
            {
                return Content("Access denied", "text/html");
            }

            if (!result.Succeeded) return View(user);

            ViewData["ReturnUrl"] = returnUrl;
            
            if (string.IsNullOrEmpty(returnUrl))
            {
                return RedirectToAction("Index","Recipes");
            }

            return Redirect(returnUrl);

        }

        [Authorize]
        [HttpGet]
        [Route("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            return RedirectToAction("Index", "Recipes");
        }

        [HttpGet]
        [Route("register")]
        public async Task<IActionResult> Register()
        {
            return View();
        }
        
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromForm] UserDTO user, string returnUrl)
        {
            Console.WriteLine(ModelState.IsValid ? "Valid" : "Not Valid");

            var result = await _userManager.CreateAsync(new User
            {
                UserName = user.UserName,
                Email = user.Email
            }, user.PasswordHash);

            if (!result.Succeeded) return View(user);
            
            var currentUser = await _userManager.FindByNameAsync(user.UserName);
            await _userManager.AddToRoleAsync(currentUser, "User");
            return RedirectToAction("Login");

        }

        [Authorize(Roles = "Administrator")]
        [Route("private")]
        public IActionResult Private()
        {
            return Content($"This is a private area. Welcome {HttpContext.User.Identity.Name}", "text/html");
        }

        [Authorize]
        [HttpGet]
        [Route("update")]
        public IActionResult Update(string form)
        {
            ViewData["form"] = form;
            return View();
        }
        
        [Authorize]
        [HttpPost]
        [Route("UpdateEmail")]
        public async Task<IActionResult> UpdateEmail([FromForm]User user)
        {
            
            var result = await _userManager.SetEmailAsync(_userManager.GetUserAsync(HttpContext.User).Result, user.Email);
 
            if (result.Succeeded)
            {
                var data = Guid.NewGuid();
                return RedirectToAction("Profile");
            }

            return View("Profile");

        }
        
        [Authorize]
        [HttpPost]
        [Route("UpdatePassword")]
        public async Task<IActionResult> UpdatePassword([FromForm]User user, String old_password)
        {
            var db_user = _userManager.GetUserAsync(HttpContext.User).Result;
            var result = await _userManager.ChangePasswordAsync(db_user, old_password, user.PasswordHash);
 
            if (result.Succeeded)
            {
                return RedirectToAction("Profile");
            }

            return View("Profile");
        }
        
    }
}