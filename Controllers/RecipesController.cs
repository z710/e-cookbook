﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using e_cookbook.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace e_cookbook.Controllers
{
    public class RecipesController : Controller
    {
        private EcookbookContext _ecookbookContext;
        private UserManager<User> _userManager;
        private SignInManager<User> _signInManager;

        public RecipesController(EcookbookContext ecookbookContext, UserManager<User> userManager,
            SignInManager<User> signInManager)
        {
            _ecookbookContext = ecookbookContext;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Index()
        {
            
            if (!_ecookbookContext.Recipes.Any())
            {
                ViewBag.no_recipes = "Nie ma jeszcze żadnych przepisów";
            }
            else
            {
                var list = _ecookbookContext.Ingredients;
             //   var list2 = _ecookbookContext.RecipeIngredientses;
                                  
                var recipe_list = _ecookbookContext.Recipes.Select(c => new Recipe()
                {
                    Id = c.Id,
                    Title = c.Title,
                    Createdat = c.Createdat,
                    Lastedit = c.Lastedit,
                    Ingredients = null,
                    RecipeIngredients = (from i in _ecookbookContext.RecipeIngredientses where i.RecipeId == c.Id select i).ToList(),
                    RecipeStep = (from s in _ecookbookContext.RecipeSteps where s.RecipeId == c.Id select s).ToList()
                    
                }).ToList();
   
                ViewBag.recipes = recipe_list;
                
                
                foreach (var items  in  recipe_list)
                {
                    var cos = (from i in _ecookbookContext.RecipeIngredientses
                        where i.RecipeId == items.Id
                        select i.IngredientId).ToList();
                    items.Ingredients = _ecookbookContext.Ingredients.Where(s => cos.Contains(s.Id)).ToList();
                }
                //  ViewBag.ingredients = _ecookbookContext.Ingredients.ToList();
            }
            

            return View();
        }

        [Route("recipe/{id}")]
        public async Task<IActionResult> Recipe(string Id)
        {
            var id = new Guid(Id).ToString();
            var recipe = _ecookbookContext.Recipes.Where(Recipes => Recipes.Id == id).ToList();

            if (recipe.Count != 1)
            {
                ViewBag.noRecipe = "Brak przepisu o podanym ID";

                return View();
            }

            var comments = _ecookbookContext.Comments.Where(Comments => Comments.Recipe.Id == id).ToList();


            var stepList = new List<string>();
            var steps = _ecookbookContext.RecipeSteps.Where(Steps => Steps.RecipeId == id).ToList();

            foreach (var step in steps)
            {
                stepList.Add(step.Text);
            }


            var rating = _ecookbookContext.RecipeRating.Where(recipeRating => recipeRating.Recipe.Id == id)
                .ToList();

            var recipeRatingSum = 0;
            foreach (var rate in rating)
            {
                recipeRatingSum += rate.Rating;
            }

            var recipeRatingAvg = 0;
            if (rating.Count != 0)
            {
                recipeRatingAvg = recipeRatingSum / rating.Count;
            }

            var ingredientList = _ecookbookContext.RecipeIngredientses.Where(r => r.Recipes.Id == id).ToList();

            var ingredients = new List<string>();

            foreach (var ingredient in ingredientList)
            {
                var tmp = _ecookbookContext.Ingredients.Single(r => r.Id == ingredient.Id);
                ingredients.Add(tmp.Name);
            }

            var recipeDetails = new RecipeDetailsDTO
            {
                Title = recipe[0].Title, Id = recipe[0].Id, Comments = comments, Step = stepList,
                Rating = recipeRatingAvg, Ingredients = ingredients
            };


            ViewBag.recipe = recipeDetails;

            return View();
        }

        [Authorize]
        [HttpGet]
        [Route("addrecipe")]
        public async Task<IActionResult> AddRecipe()
        {
            var ingredients = await _ecookbookContext.Ingredients.ToListAsync();
            ViewBag.ingredients = ingredients;
            return View();
        }

        [Authorize]
        [HttpPost]
        [Route("addrecipe")]
        public async Task<IActionResult> AddRecipe([FromForm] Recipe recipe, ICollection<String>  RecipeIngredients, ICollection<String> RecipeSteps)
        {
/*            var ingredients = await _ecookbookContext.Ingredients.ToListAsync();
            ViewBag.ingredients = ingredients;*/

            var userId = _userManager.GetUserId(HttpContext.User);


            var userRecipe = new Recipe();


            var title = recipe.Title;
            var createdAt = new DateTime();
            createdAt = DateTime.Now;

            var recipeId = Guid.NewGuid().ToString();

            userRecipe.Id = recipeId;
            userRecipe.UserId = userId;

            userRecipe.Title = title;
            userRecipe.Createdat = createdAt;


            _ecookbookContext.Recipes.Add(userRecipe);

            foreach (var currentStep in RecipeSteps)
            {
                if (currentStep == null) continue;

                var recipeSteps = new RecipeStep();
                var guid = Guid.NewGuid().ToString();

                recipeSteps.Id = guid;
                recipeSteps.RecipeId = recipeId;
                recipeSteps.Text = currentStep;

                _ecookbookContext.RecipeSteps.Add(recipeSteps);
            }

            foreach (var ingredient in RecipeIngredients)
            {
                if (ingredient == null) continue;
                
                var recipeIngredients = new RecipeIngredient();
                var guid = Guid.NewGuid().ToString();

                recipeIngredients.Id = guid;
                recipeIngredients.RecipeId = recipeId;
                recipeIngredients.IngredientId = ingredient;

                _ecookbookContext.RecipeIngredientses.Add(recipeIngredients);
            }

            await _ecookbookContext.SaveChangesAsync();

            return RedirectToAction("Recipe", new {id = recipeId});
        }


        [Authorize]
        [HttpGet]
        [Route("addingredient")]
        public IActionResult AddIngredient()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [Route("addingredient")]
        public async Task<IActionResult> AddIngredient([FromForm] Ingredient ingredient)
        {
            var _ingredient = _ecookbookContext.Ingredients.FirstOrDefault(i => i.Name == ingredient.Name);
            if (_ingredient == null)
            {
                ingredient.Id = Guid.NewGuid().ToString();
                await _ecookbookContext.Ingredients.AddAsync(ingredient);
                await _ecookbookContext.SaveChangesAsync();
                ViewBag.result = "Dodano składnik";
            }
            else
            {
                ViewBag.result = "Składnik już istnieje";
            }

            return View();
        }

        [Authorize]
        [HttpPost]
        [Route("addcomment")]
        public async Task<IActionResult> AddComment([FromForm] Comment comment, string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return Redirect(returnUrl);
        }
    }
}