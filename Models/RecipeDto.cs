using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace e_cookbook.Models
{
    public class RecipeDto
    {
        public string Title { get; set; }
        public List<string> Step { get; set; }
        public List<string> Ingredients { get; set; }
        
    }
}