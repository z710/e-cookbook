﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$(function() {

    var InputsWrapper   = $("#fields");
    var x = InputsWrapper.length;
    var FieldCount=1;


    $(document).on("click", ".add", function(e){
        FieldCount++;
        $(InputsWrapper).append('<div class="newField"><label asp-for="RecipeStep">Następny krok</label><textarea onkeyup="textAreaAdjust(this)" asp-for="RecipeStep" name="RecipeSteps" class="step" id="RecipeStep['+ FieldCount +']"> </textarea><button type="button" class=" btn btn-outline-dark my-sm-2 remove">Remove</button></div></div>');
        x++;
        return false;
    });

    $(document).on("click",".remove", function(e){
        if( x > 1 ) {
            $(this).parent('div').remove();
            x--;
        }
        return false;
    });



    var InputsWrapper2   = $("#ingredients");
    var z = InputsWrapper2.length;
    var FieldCount2=1;


    $(document).on("click", ".add-ingredient", function(e){
        FieldCount2++;
        var t = $('#select :selected').text();
        $(InputsWrapper2).append('<li>'+ t +'<input type="hidden" value="'+$('#select :selected').val()+'" asp-for="RecipeIngredients" id="RecipeIngredients['+ FieldCount2 +']" name="RecipeIngredients" /><i class="fa fa-minus-circle remove-ingredient ml-1"></i></li>');
        z++;
        return false;
    });

    $(document).on("click",".remove-ingredient", function(e){
        if( z > 1 ) {
            $(this).parent('li').remove();
            z--;
        }
        return false;
    });
});

// wyswietlanie okna z szukajka via przycisk
$("#search-button").click(function(){
    $("#search-div").toggle();
});

// auto rozszerzanie teztarea
function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (25+o.scrollHeight)+"px";
}

function addIngredient(o,t) {
    $("#ingredients").append('<li>'+ t +'</li><input type="hidden" value="'+t+'" asp-for="Ingredients" name="ingredients[]" /><i class="fa fa-minus-circle"></i>');
}