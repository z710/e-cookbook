using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_cookbook.Models
{
    public class RecipeIngredient
    {
        public string Id { get; set; }
        public string IngredientId { get; set; }
        public string RecipeId { get; set; }
        
        public Ingredient Ingredients { get; set; }
        public Recipe Recipes { get; set; }
    }
}