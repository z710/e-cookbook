﻿using System;

namespace e_cookbook.Models
{
    public class RecipeStep
    {
        public string Id { get; set; }
        public string Text { get; set; }
        
        public string RecipeId { get; set; }

        public Recipe Recipe { get; set; }
    }
}
