namespace e_cookbook.Models
{
    public class UsersListDto
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string RoleId { get; set; }
        public string Role { get; set; }
    }
}