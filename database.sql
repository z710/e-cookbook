
-- Należy tworzyć bazę i połączyć się z nią, ponieważ Postgresql nie umożliwia zmiany bazy danych
-- CREATE database eCookbook

CREATE EXTENSION IF NOT EXISTS "uuid-ossp"

SELECT to_char(current_timestamp, 'YYYY-MM-DD"T"HH24:MI:SS:MS"Z"')

SELECT uuid_generate_v4();

CREATE TABLE users(
Id UUID DEFAULT uuid_generate_v4(),
Name VARCHAR(50) NOT NULL,
Email VARCHAR(100) NOT NULL,
Password VARCHAR(255) NOT NULL,
PRIMARY KEY(Id)
)

CREATE TABLE roles(
Id UUID DEFAULT uuid_generate_v4(),
Name VARCHAR(50) NOT NULL,
PRIMARY KEY(Id)
)

CREATE TABLE userRoles(
Id UUID DEFAULT uuid_generate_v4(),
RoleID UUID REFERENCES roles(id),
UserId UUID REFERENCES users(id),
PRIMARY KEY(Id)
)

CREATE TABLE recipes(
Id UUID DEFAULT uuid_generate_v4(),
Title varchar(100) NOT NULL,
CreatedAt TIMESTAMP NOT NULL,
LastEdit TIMESTAMP NOT NULL,
UserId UUID NOT NULL REFERENCES users(id),
PRIMARY KEY(Id)
)


CREATE TABLE comments(
Id UUID DEFAULT uuid_generate_v4(),
UserID UUID NOT NULL REFERENCES users(id),
RecipeId UUID NOT NULL REFERENCES recipes(id),
CreatedAT TIMESTAMP NOT NULL,
Comment VARCHAR(255) NOT NULL,
PRIMARY KEY(Id)
)


CREATE TABLE visibility(
Id UUID DEFAULT uuid_generate_v4(),
Name VARCHAR(100) NOT NULL,
PRIMARY KEY(Id)
)

CREATE TABLE recipeRating(
Id UUID DEFAULT uuid_generate_v4(),
UserID UUID NOT NULL REFERENCES users(id),
RecipeId UUID NOT NULL REFERENCES recipes(id),
Rating Integer NOT NULL,
PRIMARY KEY(Id)
)

CREATE TABLE recipeSteps(
Id UUID DEFAULT uuid_generate_v4(),
RecipeId UUID NOT NULL REFERENCES recipes(id),
Text VARCHAR(255) NOT NULL,
PRIMARY KEY(Id)
)

CREATE TABLE ingredients(
Id UUID DEFAULT uuid_generate_v4(),
Name VARCHAR(100) NOT NULL,
PRIMARY KEY(Id)
)

CREATE TABLE recipeIngredients(
UserId UUID NOT NULL REFERENCES users(id),
RecipeId UUID NOT NULL REFERENCES  recipes(id)
)

CREATE TABLE logs(
Id UUID DEFAULT uuid_generate_v4(),
UserId UUID NOT NULL REFERENCES  users(id),
RecipeId UUID NOT NULL REFERENCES recipes(id),
TIMESTAMP TIMESTAMP NOT NULL,
PRIMARY KEY(Id)
)