using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices.WindowsRuntime;

namespace e_cookbook.Models
{
    public class UserDTO
    {   
        [StringLength(100)]
        [MinLength(5)]
        [Required] 
        public string UserName { get; set; }
        [EmailAddress]
        [StringLength(100)]
        [Required] 
        public string Email { get; set; }
        [Required] 
        public string PasswordHash { get; set; }
    }
}