﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_cookbook.Models
{
    public class Ingredient
    {
        public string Id { get; set; }
     
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        
        public List<RecipeIngredient> RecipeIngredients { get; set; }
    }
}
