﻿using System;

namespace e_cookbook.Models
{
    public class Comment
    {
        public string Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Text { get; set; }

        public Recipe Recipe { get; set; }
        public User User { get; set; }
    }
}
