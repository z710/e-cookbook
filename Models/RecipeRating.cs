﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_cookbook.Models
{
    public class RecipeRating
    {
        public string Id { get; set; }
        public string Userid { get; set; }
        public int Rating { get; set; }
        public string RecipeId { get; set; }
        public string UserId { get; set; }

        public Recipe Recipe { get; set; }
        public User User { get; set; }
    }
}
