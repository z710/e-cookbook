﻿using System;

namespace e_cookbook.Models
{
    public class Log
    {
        public string Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string UserId { get; set; }
        public Guid RecipeId { get; set; }

        public Recipe Recipe { get; set; }
        public User User { get; set; }
    }
}
