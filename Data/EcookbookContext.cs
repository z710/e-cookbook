﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace e_cookbook.Models
{
    public class EcookbookContext : IdentityDbContext<User>
    {
        public EcookbookContext()
        {
        }

        public EcookbookContext(DbContextOptions<EcookbookContext> options)
            : base(options)
        {
        }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<RecipeRating> RecipeRating { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<RecipeStep> RecipeSteps { get; set; }
        public DbSet<RecipeIngredient> RecipeIngredientses { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            {
//                modelBuilder.HasPostgresExtension("uuid-ossp")
//                    .HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

                //sekcja ról
                modelBuilder.Entity<Role>().HasData(new Role
                    {Id = "eeb317ae-0771-4806-841c-420543d958fe", Name = "Administrator", NormalizedName = "ADMINISTRATOR"});
                modelBuilder.Entity<Role>().HasData(new Role
                    {Id = "f6151371-28cd-4b6e-bb57-24873ff498ac", Name = "User", NormalizedName = "USER"});
                
                // sekcja skladnikow
                modelBuilder.Entity<Ingredient>().HasData(new Ingredient
                {
                    Id = "dcff0df1-5c34-4abc-9bb9-e09bfabe74ff",
                    Name = "Pomidory"
                });
                modelBuilder.Entity<Ingredient>().HasData(new Ingredient
                {
                    Id = "7f51ad11-5728-484d-bed9-c9dcddd64737",
                    Name = "Ogórki"
                });
                modelBuilder.Entity<Ingredient>().HasData(new Ingredient
                {
                    Id = "06b0a454-9b69-425d-8987-3c655e38ce1e",
                    Name = "Ziemniaki"
                });
                modelBuilder.Entity<Ingredient>().HasData(new Ingredient
                {
                    Id = "531e7327-d0b9-4d83-86aa-845881d7b423",
                    Name = "Cebula"
                });


                // sekcja relacji
                modelBuilder.Entity<Recipe>().HasMany(s => s.RecipeStep);
            }
        }
    }
}