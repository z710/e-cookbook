using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using e_cookbook.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace e_cookbook.Controllers
{
    [Authorize(Roles = "Administrator")]
    [Route("[controller]")]
    public class AdminController : Controller
    {
        private EcookbookContext _ecookbookContext;
        private UserManager<User> _userManager;
        private SignInManager<User> _signInManager;
        private RoleManager<IdentityRole> _roleManager;

        public AdminController(EcookbookContext context, UserManager<User> userManager,
            SignInManager<User> signInManager, RoleManager<IdentityRole> roleManager)
        {
            _ecookbookContext = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet]
        [Route("UserManagement")]
        public async Task<IActionResult> UserManagement()
        {
            var Users = _userManager.Users.ToList();
            var UsersList = new List<UsersListDto>();
            foreach (var var in Users)
            {
                var user = new UsersListDto();
                user.Id = var.Id;
                user.UserName = var.UserName;
                user.Email = var.Email;
                
                var userRole = await _userManager.GetRolesAsync(var);
                user.Role = userRole[0];
                user.RoleId = userRole[0] == "Administrator" ? "1" : "2";
                
                UsersList.Add(user);
            }
            ViewBag.allUsers = UsersList;
            return View();
        }

        [HttpGet]
        public IActionResult UserEdit(object id)
        {
            throw new System.NotImplementedException();
        }

        [HttpGet]
        public IActionResult UserBlock(object id)
        {
            throw new System.NotImplementedException();
        }

        [HttpGet]
        public IActionResult UserDelete(object id)
        {
            throw new System.NotImplementedException();
        }
        
        [HttpGet]
        [Route("RoleManagement")]
        public async Task<IActionResult> RoleManagement()
        {
            var AllRoles = _roleManager.Roles;
            ViewBag.allRoles = AllRoles;
            return View();
        }
        
        [HttpGet]
        public IActionResult RoleCreate()
        {
            throw new System.NotImplementedException();
        }
        
        [HttpGet]
        public IActionResult RoleEdit()
        {
            throw new System.NotImplementedException();
        }
        
        [HttpGet]
        public IActionResult RoleDelete()
        {
            throw new System.NotImplementedException();
        }

    }
}