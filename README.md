# README #

E-Cookbook

Diagramy z folderu diagrams są z serwisu draw.io, gdzie można ponownie zaimportować XMLe z diagramem i go edytować w prosty sposób.


### Serwis E-Cookbook - Core ###

* Przepisy mają trzy statusy: publiczny, prywatny i dla zalogowanych
* Autoryzacja
* Dodawanie przepisów przez zalogowanych użytkowników
* Ocena przepisu przez zalogowanych użytkowników
* Komentarz/Review przepisu przez zalogowanych użytkowników

### Baza danych zawiera ###

* Przepisy
* Użytkowników
* Logi


## ASP.NET Core na linuksie
### Instalacja .net core na linuksie
Należy pobrać dotnet sdk np. ze strony https://dotnet.microsoft.com/download . Do dystrybcji jak debian czy fedora jest instrukcja krok po kroku.
Na Manjaro jest gotowa paczka w repozytorium, którą można zainstalować za pomocą:

> sudo pacman -S dotnet-sdk

Po pomyślnej instalacji możemy utworzyć projekt z szablonu, np. jak w naszym przypadku MVC:

> dotnet new mvc

### IDE

Istnieje wiele IDE dostępnych na linuksa do edycji kodu. Do C# jednym z lepszych jest Rider od znanego producenta stojącym za takimi produktami jak Inteliji, PyCharm czy ReSharper. Studenci mogą korzystać z produktów za darmo.

Wygodnym narzędziem do zarządzania produktami JetBrainsów jest toolbox do pobrania z oficjalnej strony https://www.jetbrains.com/toolbox/ Istnieje wersja na windowsa, linuksa jak i MacOS.
Aby zainstalować toolboxa na linuksie, wystarczy wypadkować instalator za pomocą np.

> tar -xvf <nazwa_archiwum>.tar.gz

I następnie go uruchomić

> ./<nazwa_wypakowanego_pliku>

Jeżeli Rider nie wykryje dotnet sdk, należy manualnie podać jego ścieżkę Aby to zrobić należy przejść do File -> Settings... -> Build, Execution, Deployment -> Toolset and Build i w polu .NET Core CLI executable path podać ścieżkę do pliku dotnet. W moim przypadku było to:
> /opt/dotnet/dotnet

### Postgresql

Instalacja lokalnego Postgresql na linuksie jest banalna. W zależności od repozytorium powinna zająć kilka komend. Przykład instalacji w Manjaro:

Instalacja postgresql poprzez pacmana:

> sudo pacman -S postgresql

Następnie logujemy się na użytkownika postgres z parametrem -l

> sudo su postgres -l

Inicjujemy bazę danych dla PostreSQL

> initdb --locale $LANG -E UTF8 -D '/var/lib/postgres/data/'

Wracamy do naszego użytkownika

> exit

I na końcu należy włączyć naszą bazę:

> sudo systemctl enable --now postgresql.service \
> sudo systemctl start postgresql.service

### Tworzenie bazy danych e-cookbook

##### -- TODO --

### Baza danych w .NET CORE


Za pomocą entity framework możemy przenieść strukturę tabel i relacji do modeli w C#, oraz utworzone zostanie DbContext. Jest to podejście database first. Przykładowo w tym projekcie było to:

> dotnet ef dbcontext scaffold "Host=localhost; Database=ecookbook; Username=postgres; Password=ProjektZespolowy" Npgsql.EntityFrameworkCore.PostgreSQL -o Models


#### TEST AUTOBUILDU #####

Test push to check integration