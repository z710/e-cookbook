using System;
using System.Collections.Generic;

namespace e_cookbook.Models
{
    public class RecipeDetailsDTO
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public List<string> Step { get; set; }
        public List<string> Ingredients { get; set; }
        public int Rating { get; set; }
        public List<Comment> Comments { get; set; }
    }
}